import { Component } from "react";

import likeImg from "../../../assets/images/like.png";

class LikeImage extends Component {
    render() {
        return (
            <div className="row mt-3">
                <p>Thông điệp ở đây</p>
                <img src={likeImg} alt="like" style={{width: "100px", margin: "0 auto"}}/>
            </div>
        )
    }
}

export default LikeImage;