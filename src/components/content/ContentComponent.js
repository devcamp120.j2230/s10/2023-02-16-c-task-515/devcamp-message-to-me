import React, { Component } from "react";
import InputMessage from "./input-message/InputMessage";
import LikeImage from "./like-image/LikeImage";

class ContentComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <InputMessage />
                <LikeImage />
            </React.Fragment>
        )
    }
}

export default ContentComponent;